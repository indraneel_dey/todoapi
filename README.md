# TODOApi

A simple API that allows to build a to do list

## Requirements
 - The newest Net Core SDK found at [Microsoft](https://www.microsoft.com/net/download)
 - A web browser of your choosing
 
## Running the program
1. In terminal go to /Todo.Api
2. Run command _**dotnet run**_
3. To go to the Swagger page: http://localhost:5000/index.html


## Unit Testing
All unit tests were built using [NUnit](https://nunit.org/).

The unit tests were run through JetBrains Rider, and would work
through Visual Studio as well with the proper extension. 

## Future Updates
- Continue to add features
- Dockerize the project
