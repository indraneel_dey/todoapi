﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Todo.API.CRUD;
using Todo.API.Helpers;
using Todo.API.Models;
using Todo.Database;

namespace Todo.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ListController : ControllerBase
    {
        /// <summary>
        /// Gets all lists
        /// </summary>
        /// <returns><see cref="ReturnModel"/></returns>
        [HttpGet]
        public ReturnModel GetList()
        {
            try
            {
                var results = Retrive.GetAllLists();

                return APIHelper.MultipleReturn(results);
            }
            catch (Exception ex)
            {
                return new ReturnModel
                {
                    code = HttpStatusCode.InternalServerError,
                    data = new JArray(),
                    message = ex.ToString()
                };
            }
        }

        /// <summary>
        /// Creates a list
        /// </summary>
        /// <param name="model"><see cref="ListModel"/></param>
        /// <returns><see cref="ReturnModel"/></returns>
        [HttpPost]
        public ReturnModel CreateList(ListModel model)
        {
            try
            {
                var results = Create.AddList(model);
                return APIHelper.SingleReturn(results, true);
            }
            catch (Exception ex)
            {
                return new ReturnModel
                {
                    code = HttpStatusCode.InternalServerError,
                    data = new JArray(),
                    message = ex.ToString()
                };
            }
        }

        /// <summary>
        /// Gets a list by id
        /// </summary>
        /// <param name="id">Id of the list</param>
        /// <returns><see cref="ReturnModel"/></returns>
        [HttpGet]
        [Route("{id}")]
        public ReturnModel GetListById(string id)
        {
            try
            {
                var results = Retrive.GetListById(id);

                return APIHelper.SingleReturn(results);
            }
            catch (Exception ex)
            {
                return new ReturnModel
                {
                    code = HttpStatusCode.InternalServerError,
                    data = new JArray(),
                    message = ex.ToString()
                };
            }
        }

        [HttpPost]
        [Route("{id}/tasks")]
        public ReturnModel AddTaskToList(string id, TaskModel model)
        {
            try
            {

                var result = Update.AddTaskToList(id, model);
                return APIHelper.SingleReturn(result);
            }
            catch (Exception ex)
            {
                return new ReturnModel
                {
                    code = HttpStatusCode.InternalServerError,
                    data = new JArray(),
                    message = ex.ToString()
                };
            }
        }

        [HttpPost]
        [Route("{id}/task/{taskId}/complete")]
        public ReturnModel TaskToComplete(string id, string taskId, CompletedModel model)
        {
            try
            {
                if (model.complete != true)
                {
                    return new ReturnModel
                    {
                        code = HttpStatusCode.BadRequest,
                        data = new JArray(),
                        message = "Task not set to completed"
                    };
                }
                
                
                var list = Retrive.GetListById(id);
                if (list == null)
                {
                    return new ReturnModel
                    {
                        code = HttpStatusCode.NotFound,
                        data = new JArray(),
                        message = "No List found"
                    };
                }
                
                var result = Update.CompleteTask(taskId);
                
                return APIHelper.SingleReturn(result);
            }
            catch (Exception ex)
            {
                return new ReturnModel
                {
                    code = HttpStatusCode.InternalServerError,
                    data = new JArray(),
                    message = ex.ToString()
                };
            }

         
        }
    }
}