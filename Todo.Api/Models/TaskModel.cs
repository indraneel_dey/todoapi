﻿using System.ComponentModel.DataAnnotations;

namespace Todo.API.Models
{
    public class TaskModel
    {
        [Required]
        public string id { get; set; }
        public string name { get; set; }
        public bool completed { get; set; }
        //public int listId { get; set; }
        //public ListModel list { get; set; }
    }
}