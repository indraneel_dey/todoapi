﻿using System.Net;
using Newtonsoft.Json.Linq;

namespace Todo.API.Models
{
    public class ReturnModel
    {
        public HttpStatusCode code { get; set; }
        public string message { get; set; }
        public JArray data { get; set; }
    }
}