﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Todo.API.Models
{
    /// <summary>
    /// List model
    /// </summary>
    public class ListModel
    {
        /// <summary>
        /// Identification
        /// </summary>
        [Required]
        public string id { get; set; }
        
        /// <summary>
        /// Name
        /// </summary>
        [Required]
        public string name { get; set; }
        
        /// <summary>
        /// Description
        /// </summary>
        public string description { get; set; }
        
        /// <summary>
        /// Tasks in list
        /// </summary>
        public List<TaskModel> tasks { get; set; }
    }
}