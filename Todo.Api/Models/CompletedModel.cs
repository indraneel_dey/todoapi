﻿namespace Todo.API.Models
{
    public class CompletedModel
    {
        public bool complete { get; set; } = false;
    }
}