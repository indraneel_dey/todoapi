﻿using System;
using AutoMapper;
using Todo.API.AutoMapper.Profiles;
using Todo.API.Models;
using Todo.Database;
using Todo.Database.Models.Database;

namespace Todo.API.CRUD
{
    public static class Update
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Adds a Task to a particular list
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static TaskModel AddTaskToList(string id, TaskModel model)
        {
            try
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<TaskDbtoJsonProfile>();
                    cfg.AddProfile<ListDbToJsonProfile>();
                });
                
                var mapper = config.CreateMapper();

                var task = mapper.Map<TaskDbModel>(model);
                
                var result = DbSingleton.AddTaskToList(task, id);

                if (result == null) return null;
                
                var foundTask = DbSingleton.FindTaskByExternalId(result.ExternalId, true);
                var mapped = mapper.Map<TaskModel>(foundTask);
                
                return mapped;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }

        /// <summary>
        /// Turns a task to complete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static TaskModel CompleteTask(string id)
        {
            try
            {
                var task = DbSingleton.FindTaskByExternalId(id);

                if (task == null) return null;
                task.Complete = true;

                task = DbSingleton.UpdateTask(task);
                
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<TaskDbtoJsonProfile>();
                });
                
                var mapper = config.CreateMapper();
                var mappedTask = mapper.Map<TaskModel>(task);

                return mappedTask;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;

        }
    }
}