﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Todo.API.AutoMapper.Profiles;
using Todo.API.Models;
using Todo.Database;
using Todo.Database.Models.Database;

namespace Todo.API.CRUD
{
    public static class Create
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Adds a list to the database
        /// </summary>
        /// <param name="model"><see cref="ListModel"/></param>
        /// <returns><see cref="ListDbModel"/></returns>
        public static ListModel AddList(ListModel model)
        {
            try
            {
                
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<TaskDbtoJsonProfile>();
                    cfg.AddProfile<ListDbToJsonProfile>();
                    cfg.AddProfile<ListJsonToDbProfile>();
                });

                var mapper = config.CreateMapper();

                var list = mapper.Map<ListDbModel>(model);

                List<TaskDbModel> createdTasks = new List<TaskDbModel>();
                
                var createdList = DbSingleton.CreateList(list);
                
                if (model.tasks != null && model.tasks?.Count != 0)
                {
                    
                    model.tasks.ForEach(o =>
                    {
                       //Adds the task to the listing
                        var task = mapper.Map<TaskDbModel>(o); 
                        createdTasks.Add(DbSingleton.CreateTask(task));
                    });
                    
                    createdList.Tasks = createdTasks;
                }

                var mapped = mapper.Map<ListModel>(createdList);

                return mapped;
            }
            catch (NullReferenceException ex)
            {
                Log.Warn(ex);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }
    }
}