﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Todo.API.AutoMapper.Profiles;
using Todo.API.Models;
using Todo.Database;

namespace Todo.API.CRUD
{
    public static class Retrive
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Gets all the lists
        /// </summary>
        /// <returns></returns>
        public static List<ListModel> GetAllLists()
        {
            try
            {
               
                var result = DbSingleton.GetAllLists();

                if (result.Count == 0)
                {
                    return new List<ListModel>();
                }

                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<ListDbToJsonProfile>();
                    cfg.AddProfile<TaskDbtoJsonProfile>();
                });
                var mapper = config.CreateMapper();
                var mapped = mapper.Map<List<ListModel>>(result);
                return mapped;
            }
            catch (NullReferenceException ex)
            {
                Log.Warn(ex);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }
            
            return null;
        }

        /// <summary>
        /// Gets a single list by its external id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static ListModel GetListById(string id)
        {
            try
            {
                var result = DbSingleton.FindListByExternalId(id);

                if (result == null)
                {
                    return null;
                }
                
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<ListDbToJsonProfile>();
                    cfg.AddProfile<TaskDbtoJsonProfile>();
                });
                var mapper = config.CreateMapper();
                var mapped = mapper.Map<ListModel>(result);
                
                return mapped;
            }
            catch (NullReferenceException ex)
            {
                Log.Warn(ex);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }
            
            return null;
        }
    }
}