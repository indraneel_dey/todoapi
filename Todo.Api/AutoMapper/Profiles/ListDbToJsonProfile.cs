﻿using AutoMapper;
using Todo.API.Models;
using Todo.Database.Models.Database;

namespace Todo.API.AutoMapper.Profiles
{
    /// <summary>
    /// List Profile for Db to Json
    /// </summary>
    public class ListDbToJsonProfile:Profile
    {
        public ListDbToJsonProfile()
        {
           
            CreateMap<ListDbModel, ListModel>()
                .ForMember(d => d.name, o => o.MapFrom(s => s.Name))
                .ForMember(d => d.description, o => o.MapFrom(s => s.Description))
                .ForMember(d => d.id, o => o.MapFrom(s => s.ExternalId))
                .ForMember(d=>d.tasks, o=> o.MapFrom(s=>s.Tasks))
                .ForAllOtherMembers(d=>d.Ignore());
        }
    }
}