﻿using System.Data.Common;
using AutoMapper;
using Todo.API.Models;
using Todo.Database;
using Todo.Database.Models.Database;

namespace Todo.API.AutoMapper.Profiles
{
    /// <summary>
    /// List Json to Db profile
    /// </summary>
    public class ListJsonToDbProfile: Profile
    {
        public ListJsonToDbProfile()
        {
            CreateMap<TaskModel, TaskDbModel>()
                .ForMember(d=>d._id, o=>o.MapFrom(s=>FindTaskId(s.id)))
                .ForMember(d=>d.ExternalId, o=>o.MapFrom(s=>s.id))
                .ForMember(d=>d.Complete, o=>o.MapFrom(s=>s.completed))
                .ForMember(d=>d.Title, o=>o.MapFrom(s=>s.name));
            
            CreateMap<ListModel, ListDbModel>()
                .ForMember(d=>d._id, o=>o.MapFrom(s=>FindListId(s.id)))
                .ForMember(d=>d.ExternalId, o=>o.MapFrom(s=>s.id))
                .ForMember(d=>d.Description, o=>o.MapFrom(s=>s.description))
                .ForMember(d=>d.Name, o=>o.MapFrom(s=>s.name));
        }

        private static int FindListId(string externalId)
        {
            var list = DbSingleton.FindListByExternalId(externalId);
            return list?._id ?? 0;
        }
        
        private static int FindTaskId(string externalId)
        {
            var task = DbSingleton.FindTaskByExternalId(externalId);
            return task?._id ?? 0;
        }
    }
}