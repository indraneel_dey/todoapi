﻿using AutoMapper;
using Todo.API.Models;
using Todo.Database.Models.Database;

namespace Todo.API.AutoMapper.Profiles
{
    /// <summary>
    /// Task profile mapper from db to JSON object
    /// </summary>
    public class TaskDbtoJsonProfile: Profile
    {
        public TaskDbtoJsonProfile()
        {
            CreateMap<TaskDbModel, TaskModel>()
                .ForMember(d => d.name, o => o.MapFrom(s => s.Title))
                .ForMember(d => d.completed, o => o.MapFrom(s => s.Complete))
                .ForMember(d => d.id, o => o.MapFrom(s => s.ExternalId))
                .ReverseMap()
                .ForAllOtherMembers(d=>d.Ignore());
        }
    }
}