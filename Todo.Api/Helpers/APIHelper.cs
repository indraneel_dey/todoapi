﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Microsoft.CodeAnalysis.Operations;
using Newtonsoft.Json.Linq;
using Todo.API.Models;

namespace Todo.API.Helpers
{
    /// <summary>
    /// Api helper class
    /// </summary>
    public static class APIHelper
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static TextInfo _textInfo = new CultureInfo("en-US", false).TextInfo;
        
        /// <summary>
        /// Return model builder for lists or tasks, takes in a generic
        /// </summary>
        /// <param name="multiple"></param>
        /// <param name="created"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ReturnModel MultipleReturn<T>(List<T> multiple, bool created=false)
        {
            try
            {
                var model = new ReturnModel();
                string type = typeof(T).Name == nameof(ListModel) ? "lists" : "tasks";
                
                var switchDic = new Dictionary<Func<List<T>, bool>, Action>()
                {
                    {
                        x => (x == null), () =>
                        {
                            
                            model = new ReturnModel()
                            {
                                code = HttpStatusCode.InternalServerError,
                                message = $"Error in retrieving {type}",
                                data = new JArray()
                            };
                        }
                    },
                    {
                        x => (x.Count == 0), () =>
                        {
                            model = new ReturnModel()
                            {
                                code = HttpStatusCode.NotFound,
                                message = $"No {type} found",
                                data = new JArray()
                            };
                        }
                    },
                    {
                        x => (x.Count >= 1), () =>
                        {
                            var message = String.Format("{0} {1}", _textInfo.ToTitleCase(type),
                                (created) ? "created" : "found");
                            
                            model = new ReturnModel()
                            {
                                code = (created)? HttpStatusCode.Created:HttpStatusCode.Found,
                                message = message,
                                data =  JArray.FromObject(multiple)
                            };
                        }
                    }
                };
            
                switchDic.First(kvp => kvp.Key(multiple)).Value();
                
                return model;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }
        
        /// <summary>
        /// Return Model builder, for a list or task, takes in a generic
        /// </summary>
        /// <param name="single"></param>
        /// <param name="created"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static ReturnModel SingleReturn<T>(T single, bool created=false)
        {
            try
            {
                var model = new ReturnModel();
                
                string type = typeof(T).Name == nameof(ListModel) ? "list" : "task";
                
                var switchDic = new Dictionary<Func<T, bool>, Action>()
                {
                    {
                        x => (x == null), () =>
                        {
                            model = new ReturnModel()
                            {
                                code = HttpStatusCode.InternalServerError,
                                message = $"Error in retrieving {type}",
                                data = new JArray()
                            };
                        }
                    },
                    {
                        x => true , () =>
                        {
                            var message = String.Format("{0} {1}", _textInfo.ToTitleCase(type),
                                (created) ? "created" : "found");
                            
                            model = new ReturnModel()
                            {
                                code = (created)? HttpStatusCode.Created : HttpStatusCode.Found,
                                message = message,
                                data =  JArray.FromObject(new List<T>(){single})
                            };
                        }
                    }
                };
            
                switchDic.First(kvp => kvp.Key(single)).Value();
                
                return model;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }
    }
}