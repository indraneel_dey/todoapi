﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using AutoMapper;
using Bogus;
using Microsoft.VisualStudio.TestPlatform.Utilities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using Todo.API.AutoMapper.Profiles;
using Todo.API.CRUD;
using Todo.API.Models;
using Todo.Database;
using Todo.Database.Models;
using Todo.Database.Models.Database;

namespace UnitTests
{
    [TestFixture]
    public class API_UnitTests
    {
        /// <summary>
        /// Random generated lists
        /// </summary>
        private List<ListDbModel> _lists = new List<ListDbModel>();

        /// <summary>
        /// Random Generated tasks
        /// </summary>
        private List<TaskDbModel> _tasks = new List<TaskDbModel>();

        private IMapper _mapperDbToJson;
        private IMapper _mapperJsonToDb;
        
        /// <summary>
        /// Randomizer
        /// </summary>
        static Random rnd = new Random();

        public API_UnitTests()
        {
            if (File.Exists(Constants.Filedir))
            {
                File.Delete(Constants.Filedir);
            }

            //Seeder set for bogus
            Randomizer.Seed = new Random(8675309);

            var tasks = new Faker<TaskDbModel>()
                .StrictMode(true)
                .RuleFor(o => o.Complete, f => f.Random.Bool())
               
               
                .RuleFor(o => o.Title, f => f.Random.Words(8))
                .RuleFor(o => o.ExternalId, f => Guid.NewGuid().ToString())
                .Ignore(o => o._id)
               
                .Ignore(o => o.List)
                .Ignore(o=>o.ListId)
                .Ignore(o=>o.ListId)
                .Generate(10);
            _tasks = tasks;

            //Generate random lists
            var lists = new Faker<ListDbModel>()
                .StrictMode(true)
                .RuleFor(o => o.Description, f => f.Lorem.Sentences(3))
                .RuleFor(o => o.ExternalId, f => Guid.NewGuid().ToString())
                .RuleFor(o => o.Name, f => f.Person.UserName)
                .RuleFor(o => o.Tasks, f => f.PickRandom<List<TaskDbModel>>(_tasks))
                .Ignore(o => o._id)
                .Generate(5);
            _lists = lists;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<TaskDbtoJsonProfile>();
                cfg.AddProfile<ListDbToJsonProfile>();
            });
            config.AssertConfigurationIsValid();
            
            _mapperDbToJson = config.CreateMapper();
            
        }

        [Test]
        public void MapTask()
        {
            var task = _tasks[rnd.Next(_tasks.Count)];

            var mappedTask = _mapperDbToJson.Map<TaskModel>(task);
            
            Console.WriteLine(JObject.FromObject(mappedTask));
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(mappedTask);
                Assert.AreEqual(task.ExternalId, mappedTask.id);
            });
        }

        [Test]
        public void MapList()
        {
            var list = _lists[rnd.Next(_lists.Count)];

            var mappedList = _mapperDbToJson.Map<ListModel>(list);
            
            Console.WriteLine(JObject.FromObject(mappedList));
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(mappedList);
                Assert.AreEqual(list.ExternalId, mappedList.id);
            });
        }

        [Test]
        public void MapListWithTask()
        {
            var list = _lists[rnd.Next(_lists.Count)];
            var task = _tasks[rnd.Next(_tasks.Count)];

            list.Tasks = new List<TaskDbModel>{task};
            
            var mappedList = _mapperDbToJson.Map<ListModel>(list);
            
            Console.WriteLine(JObject.FromObject(mappedList));
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(mappedList);
                Assert.AreEqual(list.ExternalId, mappedList.id);
                Assert.IsNotNull(mappedList.tasks);
            });
        }

        [Test]
        public void CreateList_NoTask()
        {
            var list = _lists[rnd.Next(_lists.Count)];
            list.Tasks = null;

            var mappedList = _mapperDbToJson.Map<ListModel>(list);

            var results = Create.AddList(mappedList);

            Console.WriteLine(JObject.FromObject(results));
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(results);
                Assert.IsInstanceOf<ListModel>(results);
                Assert.IsEmpty(results.tasks);
            });
        }

        [Test]
        public void CreateList_Task()
        {
            var list = _lists[rnd.Next(_lists.Count)];
            var task = _tasks[rnd.Next(_tasks.Count)];

            var mappedList = _mapperDbToJson.Map<ListModel>(list);
            var mappedTask = _mapperDbToJson.Map<TaskModel>(task);
            
            mappedList.tasks = new List<TaskModel> {mappedTask};
            
            var results = Create.AddList(mappedList);
            if (results != null)
            {
                Console.WriteLine(JObject.FromObject(results));
            }

            
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(results);
                Assert.IsInstanceOf<ListModel>(results);
                Assert.IsNotEmpty(results.tasks);
            });
        }

        [Test]
        public void GetAllLists()
        {
            var results = Retrive.GetAllLists();
            Console.WriteLine(JArray.FromObject(results, new JsonSerializer
            {
                Formatting = Formatting.Indented,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }));
            Assert.Multiple(() =>
            {
                Assert.IsNotNull(results);
                Assert.IsInstanceOf<List<ListModel>>(results);
                Assert.That(results.Count>=1);
            });
        }

        [Test]
        public void UpdateTask()
        {
            var list = _lists[rnd.Next(_lists.Count)];
            var task = _tasks[rnd.Next(_tasks.Count)];

            task.Complete = false;
            
            var mappedList = _mapperDbToJson.Map<ListModel>(list);
            var mappedTask = _mapperDbToJson.Map<TaskModel>(task);
            
            mappedList.tasks = new List<TaskModel> {mappedTask};
            
            var results = Create.AddList(mappedList);

            var updatedTask = Update.CompleteTask(task.ExternalId);
            
            var foundTask = DbSingleton.FindTaskByExternalId(task.ExternalId);
            Console.WriteLine(JObject.FromObject(foundTask));
            
            Assert.Multiple(() =>
            {
                Assert.AreEqual(task.ExternalId,foundTask.ExternalId);
                Assert.IsTrue(foundTask.Complete);
            });
        }
        
    }
}