using System;
using System.Collections.Generic;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Bogus;
using Newtonsoft.Json.Linq;
using NUnit;
using NUnit.Framework;
using Todo.API.CRUD;
using Todo.Database;
using Todo.Database.Models;
using Todo.Database.Models.Database;

namespace UnitTests
{
    [TestFixture]
    public class DB_UnitTests
    {
        /// <summary>
        /// Random generated lists
        /// </summary>
        private List<ListDbModel> _lists = new List<ListDbModel>();

        /// <summary>
        /// Random Generated tasks
        /// </summary>
        private List<TaskDbModel> _tasks = new List<TaskDbModel>();

        /// <summary>
        /// Randomizer
        /// </summary>
        static Random rnd = new Random();

        /// <summary>
        /// Constructor
        /// </summary>
        public DB_UnitTests()
        {
            if (File.Exists(Constants.Filedir))
            {
                File.Delete(Constants.Filedir);
            }

            //Seeder set for bogus
            Randomizer.Seed = new Random(8675309);

            //Generate random tasks
            var tasks = new Faker<TaskDbModel>()
                .StrictMode(true)
                .RuleFor(o => o.Complete, f => f.Random.Bool())
                .RuleFor(o => o.Title, f => f.Random.Words(8))
                .RuleFor(o => o.ExternalId, f => Guid.NewGuid().ToString())
                .Ignore(o => o._id)
                .Ignore(o => o.List)
                .Ignore(o => o.ListId)
                .Generate(10);
            _tasks = tasks;

            //Generate random lists
            var lists = new Faker<ListDbModel>()
                .StrictMode(true)
                .RuleFor(o => o.Description, f => f.Lorem.Sentences(3))
                .RuleFor(o => o.ExternalId, f => Guid.NewGuid().ToString())
                .RuleFor(o => o.Name, f => f.Person.UserName)
                .RuleFor(o => o.Tasks, f => f.PickRandom<List<TaskDbModel>>(_tasks))
                .Ignore(o => o._id)
                .Generate(5);
            _lists = lists;
        }

        /// <summary>
        /// Create list with no tasks
        /// </summary>
        [Test]
        public void CreateList()
        {
            var list = _lists[rnd.Next(_lists.Count)];
            list.Tasks = null;

            var result = DbSingleton.CreateList(list);

            var foundLists = DbSingleton.GetAllLists();
            Console.WriteLine(JArray.FromObject(foundLists));

            Assert.Multiple(() =>
            {
                //Assert.True(result);
                Assert.That(foundLists.Count >= 1);
                Assert.That(foundLists.First(o => o.ExternalId == list.ExternalId) != null);
                Assert.That(foundLists.FirstOrDefault()?.Tasks == null);
            });
        }

        [Test]
        public void CreateTask()
        {
            var task = _tasks[rnd.Next(_tasks.Count)];
            task.List = null;

            var result = DbSingleton.CreateTask(task);

            var foundTasks = DbSingleton.GetAllTasks();
            Console.WriteLine(JArray.FromObject(foundTasks));

            Assert.Multiple(() =>
            {
                //Assert.True(result);
                Assert.That(foundTasks.Count >= 1);
                Assert.That(foundTasks.First(o => o.ExternalId == task.ExternalId) != null);
                Assert.That(foundTasks.FirstOrDefault()?.List == null);
            });
        }


        [Test]
        public void GetAllLists()
        {
            var lists = DbSingleton.GetAllLists();
            Console.Write(JArray.FromObject(lists));

            Assert.Multiple(() =>
            {
                Assert.NotNull(lists);
                Assert.IsInstanceOf<List<ListDbModel>>(lists);
            });
        }

        [Test]
        public void GetAllTasks()
        {
            var tasks = DbSingleton.GetAllTasks();
            Console.Write(JArray.FromObject(tasks));
            Assert.Multiple(() =>
            {
                Assert.NotNull(tasks);
                Assert.IsInstanceOf<List<TaskDbModel>>(tasks);
            });
        }

        [Test]
        public void GetSpecificList()
        {
            var list = _lists[rnd.Next(_lists.Count)];
            list.Tasks = null;

            var result = DbSingleton.CreateList(list);

            var foundList = DbSingleton.FindListByExternalId(result.ExternalId);

            Console.WriteLine(JObject.FromObject(foundList));

            Assert.Multiple(() =>
            {
                Assert.IsNotNull(foundList);
                Assert.That(foundList.ExternalId == result.ExternalId);
            });
        }

        [Test]
        public void GetSpecificTask()
        {
            var task = _tasks[rnd.Next(_tasks.Count)];
            task.List = null;

            var result = DbSingleton.CreateTask(task);

            //Console.WriteLine(JObject.FromObject(result));

            var foundTask = DbSingleton.FindTaskByExternalId(result.ExternalId);

            Console.WriteLine(JObject.FromObject(foundTask));

            Assert.Multiple(() =>
            {
                Assert.That(foundTask != null);
                Assert.That(foundTask.ExternalId == result.ExternalId);
            });
        }
    }
}