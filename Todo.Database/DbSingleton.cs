﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using LiteDB;
using Todo.Database.Models;
using Todo.Database.Models.Database;

namespace Todo.Database
{
    /// <summary>
    /// The to do database as singleton
    /// </summary>
    public sealed class DbSingleton
    {
        private static readonly log4net.ILog Log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Lazy loading of class instance
        /// </summary>
        private static readonly Lazy<DbSingleton> _lazyInstance =
            new Lazy<DbSingleton>(() => new DbSingleton());

        /// <summary>
        /// Returns singleton instance
        /// </summary>
        public static DbSingleton Instance
        {
            get { return _lazyInstance.Value; }
        }

        /// <summary>
        /// Constructory
        /// </summary>
        private DbSingleton()
        {
            //Ensures on build that collection is made
            using (var db = new LiteDatabase(Constants.Filedir))
            {
                var mapper = BsonMapper.Global;

                mapper.Entity<ListDbModel>()
                    .Id(x => x._id);
                mapper.Entity<TaskDbModel>()
                    .Id(x => x._id);

                var lists = db.GetCollection<ListDbModel>(Constants.Lists);
                lists.EnsureIndex(x => x._id);
                lists.EnsureIndex(x => x.ExternalId);


                var tasks = db.GetCollection<TaskDbModel>(Constants.Tasks);
                tasks.EnsureIndex(x => x._id);
            }
        }

        #region Gets

        /// <summary>
        /// Gets all lists
        /// </summary>
        /// <returns><see cref="List"/> of <see cref="ListDbModel"/></returns>
        public static List<ListDbModel> GetAllLists()
        {
            try
            {
                using (var db = new LiteDatabase(Constants.Filedir))
                {
                    var col = db.GetCollection<ListDbModel>(Constants.Lists);
                    
                    var col2 = db.GetCollection<TaskDbModel>(Constants.Tasks);
                    
                    var results = col.Include(x=>x.Tasks).Find(Query.All(Query.Descending)).ToList();
                    results.ForEach(o =>
                    {
                        o.Tasks.AddRange(col2.Find(Query.EQ(nameof(TaskDbModel.ListId),o._id)));
                    });

                    return results;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return new List<ListDbModel>();
        }

        /// <summary>
        /// Gets all lists
        /// </summary>
        /// <returns><see cref="List"/> of <see cref="ListDbModel"/></returns>
        public static List<TaskDbModel> GetAllTasks()
        {
            try
            {
                using (var db = new LiteDatabase(Constants.Filedir))
                {
                    var col = db.GetCollection<TaskDbModel>(Constants.Tasks);
                    return col.Find(Query.All(Query.Descending)).ToList();
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return new List<TaskDbModel>();
        }

        /// <summary>
        /// Finds the list in the external id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="getTasks"></param>
        /// <returns></returns>
        public static ListDbModel FindListByExternalId(string id, bool getTasks=true)
        {
            try
            {
                using (var db = new LiteDatabase(Constants.Filedir))
                {
                    var col = db.GetCollection<ListDbModel>(Constants.Lists);
                    var col2 = db.GetCollection<TaskDbModel>(Constants.Tasks);
                    
                    ListDbModel results=results = col.FindOne(x => x.ExternalId == id);
                    if (getTasks == false)
                    {
                        results.Tasks = null;
                    }
                    else
                    {
                        results.Tasks.AddRange(col2.Find(Query.EQ(nameof(TaskDbModel.ListId), results._id)));
                    }

                    
                    
                    return results;
                }
            }
            catch (NullReferenceException ex)
            {
                Log.Warn(ex);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }

        /// <summary>
        /// Finds the list in the external id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static TaskDbModel FindTaskByExternalId(string id, bool getList=false)
        {
            try
            {
                using (var db = new LiteDatabase(Constants.Filedir))
                {
                    var col = db.GetCollection<TaskDbModel>(Constants.Tasks);
                    var result = (getList) ? col.Include(x => x.List).FindOne(x => x.ExternalId == id) :
                        col.FindOne(x => x.ExternalId == id);
                    return result;
                }
            }
            catch (NullReferenceException ex)
            {
                Log.Warn(ex);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return new TaskDbModel();
        }

        #endregion

        #region Create
        /// <summary>
        /// Creates a list in the db
        /// </summary>
        /// <param name="model"><see cref="ListDbModel"/></param>
        /// <returns><see cref="ListDbModel"/></returns>
        public static ListDbModel CreateList(ListDbModel model)
        {
            try
            {
                using (var db = new LiteDatabase(Constants.Filedir))
                {
                    var col = db.GetCollection<ListDbModel>(Constants.Lists);

                    var list = FindListByExternalId(model.ExternalId);

                    if (list != null)
                    {
                        return null;
                    }

                    col.Insert(model);
                    
                    return model;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }

        /// <summary>
        /// Creates a task in the db
        /// </summary>
        /// <param name="model"><see cref="TaskDbModel"/></param>
        /// <returns><see cref="TaskDbModel"/></returns>
        public static TaskDbModel CreateTask(TaskDbModel model)
        {
            try
            {
                using (var db = new LiteDatabase(Constants.Filedir))
                {
                    var col = db.GetCollection<TaskDbModel>(Constants.Tasks);

                    var task = FindTaskByExternalId(model.ExternalId);
                    if (task != null)
                    {
                        return null;
                    }

                    col.Insert(model);
                    return model;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }

        #endregion

        #region Add
        /// <summary>
        /// Adds a task to a list
        /// </summary>
        /// <param name="model"><see cref="TaskDbModel"/></param>
        /// <param name="listExternalId">External ID of the list</param>
        /// <returns><see cref="TaskDbModel"/></returns>
        public static TaskDbModel AddTaskToList(TaskDbModel model, string listExternalId)
        {
            try
            {
                var list = FindListByExternalId(listExternalId);
                
                if (list == null) return null;
                model.ListId = list._id;
                
                var createdTask = CreateTask(model);

                return FindTaskByExternalId(createdTask.ExternalId, true);
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;
        }


        #endregion

        #region Update
        /// <summary>
        /// Update a task
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static TaskDbModel UpdateTask(TaskDbModel model)
        {
            try
            {
                using (var db = new LiteDatabase(Constants.Filedir))
                {
                    var col = db.GetCollection<TaskDbModel>(Constants.Tasks);
                    col.Update(model);
                    return model;
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(ex);
            }

            return null;

        }

        #endregion
    }
}