﻿namespace Todo.Database.Models
{
    public class Constants
    {
        public const string Filedir = @"./database.db";
        public const string Lists = "lists";
        public const string Tasks = "tasks";
    }
}