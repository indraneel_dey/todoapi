﻿using System.Collections.Generic;

namespace Todo.Database.Models.Database
{
    /// <summary>
    /// List Database model
    /// </summary>
    public class ListDbModel
    {
        /// <summary>
        /// List Id
        /// </summary>
        public int _id { get; set; }
        
        /// <summary>
        /// External Id to be set by user
        /// </summary>
        public string ExternalId { get; set; }
        
        /// <summary>
        /// Name of List
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Description of list
        /// </summary>
        public string Description { get; set; }
        
        /// <summary>
        /// Tasks
        /// </summary>
        public List<TaskDbModel> Tasks { get; set; }
    }
}