﻿using System;

namespace Todo.Database.Models.Database
{
    /// <summary>
    /// Priority level enum for a task
    /// </summary>
    public enum Priority
    {
        Low = 1, 
        Medium = 2, 
        High = 3
    }

    /// <summary>
    /// Task Database model
    /// </summary>
    public class TaskDbModel
    {
        /// <summary>
        /// Task Id
        /// </summary>
        public int _id { get; set; }
        
        /// <summary>
        /// External Id
        /// </summary>
        public string ExternalId { get; set; }
        
        /// <summary>
        /// Title
        /// </summary>
        public string Title { get; set; }
        
        /// <summary>
        /// List Id
        /// </summary>
        public int ListId { get; set; }
        
        /// <summary>
        /// Category of task, ref Category DB Model
        /// </summary>
        public ListDbModel List { get; set; }
        
       
        /// <summary>
        /// If the task is complete, default FALSE
        /// </summary>
        public bool Complete { get; set; } = false;
    }
}